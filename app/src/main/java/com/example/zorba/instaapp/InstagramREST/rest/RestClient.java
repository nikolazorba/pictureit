package com.example.zorba.instaapp.InstagramREST.rest;



import com.example.zorba.instaapp.InstagramREST.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by torzsacristian on 29/06/2017.
 */

public class RestClient {


    // static
    private static RestClient mInstance;

    // non static

    private Retrofit retrofitAdapter;

    private RestClient(){
        initializeAdapter();
    }

    private void initializeAdapter(){
        retrofitAdapter =  new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static RestClient getInstance(){
        if(mInstance == null){
            mInstance = new RestClient();
        }
        return mInstance;
    }

    public <T> T getService(Class<T> serviceClass) {
       return retrofitAdapter.create(serviceClass);
    }
}