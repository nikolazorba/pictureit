package com.example.zorba.instaapp.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.zorba.instaapp.R;
import com.example.zorba.instaapp.dialog.ProgressSpinnerView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

public class PostActivity extends BaseActivity implements  View.OnClickListener {

    private static final int INTERNET_REQUEST = 5;
    private ImageButton imageButton;
    private EditText editDesc;
    private Uri resultUri = null;
    private StorageReference storageReference;
    private DatabaseReference databaseReference;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseUsers;
    private String userKey;
    private Button submitButton;

    private ProgressSpinnerView progressSpinnerView;

    private final int PERMISSION_ALL = 1;
    private String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        submitButton = findViewById(R.id.buttonSubmitPost);
        editDesc = findViewById(R.id.editDesc);
        firebaseAuth = FirebaseAuth.getInstance();
        imageButton = findViewById(R.id.imageButtonID);
        storageReference = FirebaseStorage.getInstance().getReference().child("PostImage");
        databaseReference = FirebaseDatabase.getInstance().getReference().child("InstaApp");

        firebaseUser =firebaseAuth.getCurrentUser();
        userKey = firebaseUser != null ? firebaseUser.getUid() : null;
        databaseUsers = FirebaseDatabase.getInstance().getReference().child("Users").child(firebaseUser.getUid());
        submitButton.setOnClickListener(this);
        progressSpinnerView = new ProgressSpinnerView(this);
        progressSpinnerView.setBackgroundColor(Color.TRANSPARENT);

    }

    public void imageButtonClicked(View view){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ALL:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setAspectRatio(1, 1)
                            .start(this);
                } else {
                    // Permission Denied
                    Toast.makeText(PostActivity.this, "Access Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();
                LinearLayout.LayoutParams layoutparams = (LinearLayout.LayoutParams) imageButton.getLayoutParams();

                layoutparams.height = 1000;

                layoutparams.width = 1000;

                imageButton.setLayoutParams(layoutparams);

                imageButton.setScaleType(ImageView.ScaleType.FIT_XY);
                Picasso.with(PostActivity.this).load(resultUri).fit().into(imageButton);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                result.getError();
            }
        } else {
                Toast.makeText(getApplicationContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }
        }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonSubmitPost:

                final String description = editDesc.getText().toString();
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.INTERNET},
                        INTERNET_REQUEST);

                if (!TextUtils.isEmpty(description)) {
                    addContentView(progressSpinnerView, new WindowManager.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                    progressSpinnerView.bringToFront();
                    progressSpinnerView.invalidate();
                    StorageReference filePath = storageReference.child(resultUri.getLastPathSegment());
                    filePath.putFile(resultUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            final Uri downloadurl = taskSnapshot.getDownloadUrl();
                            Toast.makeText(PostActivity.this, "Upload Complete", Toast.LENGTH_LONG).show();
                            final DatabaseReference newPost = databaseReference.push();

                            databaseUsers.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    newPost.child("desc").setValue(description);
                                    newPost.child("image").setValue(downloadurl.toString());
                                    newPost.child("uid").setValue(firebaseUser.getUid());
                                    newPost.child("username").setValue(dataSnapshot.child("username").getValue())
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        Intent mainIntent = new Intent(PostActivity.this, MainActivity.class);
                                                        startActivity(mainIntent);
                                                    }
                                                }
                                            });
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                        }
                    });
                } else {
                    Toast.makeText(this, "Description cannot be empty!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
