package com.example.zorba.instaapp.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.zorba.instaapp.BottomNavigationViewHelper;
import com.example.zorba.instaapp.InstagramREST.activity.FeedActivity;
import com.example.zorba.instaapp.InstagramREST.dialog.AuthenticationDialog;
import com.example.zorba.instaapp.InstagramREST.listener.AuthenticationListener;
import com.example.zorba.instaapp.POJO.Enums;
import com.example.zorba.instaapp.R;
import com.example.zorba.instaapp.dialog.CustomDialog;
import com.example.zorba.instaapp.dialog.CustomDialogBuilder;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.instabug.library.Instabug;
import com.instabug.library.invocation.InstabugInvocationEvent;


/**
 * Created by zorba on 19/12/2017.
 */

public class BaseActivity extends AppCompatActivity implements AuthenticationListener{

    private FirebaseAnalytics mFirebaseAnalytics;
    private AuthenticationDialog auth_dialog;
    private FrameLayout layoutBaseFrame;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("Lifecycle methods", " called onCreate");
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        final Instabug build = new Instabug.Builder(getApplication(), "1825da3a58f3f3b3152b62edb6903053")
                .setInvocationEvent(InstabugInvocationEvent.SHAKE)
                .build();
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(R.layout.activity_base);

        layoutBaseFrame = findViewById(R.id.content_frame);
        layoutBaseFrame.removeAllViews();
        LayoutInflater.from(this).inflate(layoutResID, layoutBaseFrame);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.tab_home:
                        Intent home = new Intent(BaseActivity.this, MainActivity.class);
                        startActivity(home);
                        break;

                    case R.id.tab_search:
                        auth_dialog = new AuthenticationDialog(BaseActivity.this, (AuthenticationListener) BaseActivity.this);
                        auth_dialog.setCancelable(true);
                        auth_dialog.show();
                        break;

                    case R.id.tab_share:
                        Intent share = new Intent(BaseActivity.this, PostActivity.class);
                        startActivity(share);
                        break;
                }
                return false;
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("Lifecycle methods", " called onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("Lifecycle methods", " called onResume");
        if (!isNetworkAvailable()) {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_LONG).show();
            new CustomDialogBuilder(this).withMessageResId(R.string.no_internet_connection).withOptions(Enums.DialogOption.SETTINGS, Enums.DialogOption.EXIT)
                    .build(new CustomDialog.OnDialogCallback() {
                        @Override
                        public void onButtonPressed(Enums.DialogOption buttonPressed) {
                            if (buttonPressed == Enums.DialogOption.SETTINGS) {
                                Intent settingsIntent = new Intent(Settings.ACTION_SETTINGS);
                                startActivity(settingsIntent);
                            } else {
                                finishAffinity();
                            }
                        }
                    });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("Lifecycle methods", " called onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("Lifecycle methods", " called onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("Lifecycle methods", " called onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("Lifecycle methods", " called odDestroy");
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onCodeReceived(String access_token) {
        if (access_token == null) {
            auth_dialog.dismiss();
        }

        Intent i = new Intent(BaseActivity.this, FeedActivity.class);
        i.putExtra("access_token", access_token);
        startActivity(i);

    }


}
