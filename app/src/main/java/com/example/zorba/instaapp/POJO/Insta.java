package com.example.zorba.instaapp.POJO;

public class Insta {

    private String  desc, image, username, uid;

    public Insta() {

    }

    public Insta(String desc, String image, String username, String uid) {
        this.desc = desc;
        this.image = image;
        this.username = username;
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDesc() {
        return desc;
    }

    public String getImage() {
        return image;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


}
