package com.example.zorba.instaapp.InstagramREST.repository;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.zorba.instaapp.InstagramREST.POJO.Data;
import com.example.zorba.instaapp.InstagramREST.POJO.DataViewModel;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

@Singleton
public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private DataRepository dataRepository;

    public ViewModelFactory(){

    }

    @Inject
    public ViewModelFactory(@NonNull DataRepository dataRepo) {
        dataRepository = dataRepo;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(DataViewModel.class)) {
            return (T) new DataViewModel(dataRepository) ;
        }
        throw new IllegalArgumentException("Unknown class name");
    }
}