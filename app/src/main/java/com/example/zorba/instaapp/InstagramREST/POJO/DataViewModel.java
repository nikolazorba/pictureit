package com.example.zorba.instaapp.InstagramREST.POJO;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.example.zorba.instaapp.InstagramREST.repository.DataRepository;

import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;


public class DataViewModel extends ViewModel {

    private LiveData<InstagramResponse> data;
    private DataRepository dataRepo;


    public DataViewModel(@Nullable  DataRepository dataRepo){
        if(this.dataRepo != null){
            return;
        }
        if(dataRepo != null){
            this.dataRepo = dataRepo;
        }

    }

    public void init(String tag, String access_token){
        if (this.data != null){

            return;
        }
        data = dataRepo.getData(tag, access_token);
    }

    public LiveData<InstagramResponse> getData() {
        if(data == null){
            data = new MutableLiveData<>();
        }
        return data;

    }
}
