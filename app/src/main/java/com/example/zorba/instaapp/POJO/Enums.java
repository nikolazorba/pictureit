package com.example.zorba.instaapp.POJO;

/**
 * Created by zorba on 27/12/2017.
 */

public class Enums {


    public enum DialogOption{

        YES ("Yes"),
        NO ("No"),
        ACCEPT("Accept"),
        SETTINGS("Settings"),
        DISMISS("Dismiss"),
        CANCEL("Cancel"),
        OK("Ok"),
        EXIT("Exit");

        private final String name;

        DialogOption(String name) {
            this.name = name;
        }

        public String toString() {
            return this.name;
        }
    }
}
