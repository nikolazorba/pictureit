package com.example.zorba.instaapp.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zorba.instaapp.POJO.Insta;
import com.example.zorba.instaapp.R;
import com.example.zorba.instaapp.adapter.RecyclerViewAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileActivity extends BaseActivity {

    private String postKey = null;
    private String postUID = null;
    private RecyclerView rvProfilePosts;
    private RecyclerViewAdapter myAdapter;
    private TextView usernameText;
    private CircularImageView circleImageView;
    private String profileImage;
    private static final int GALLERY_REQUEST = 1;
    private Uri resultUri = null;
    private String postUsername;
    private List<Map<String, Insta>> postsArr = new ArrayList<>();

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private DatabaseReference databaseReferenceUsers;
    private StorageReference storageReference;
    private String userKey = null;
    private SwipeRefreshLayout swipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        rvProfilePosts = findViewById(R.id.profilePostsID);
        rvProfilePosts.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setAutoMeasureEnabled(true);
        rvProfilePosts.setLayoutManager(linearLayoutManager);
        rvProfilePosts.setNestedScrollingEnabled(false);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);

        if (getIntent().hasExtra("PostID")) {
            postKey = getIntent().getExtras().getString("PostID");
        }
        if (getIntent().hasExtra("UserID")) {
            userKey = getIntent().getExtras().getString("UserID");
        }

        circleImageView = findViewById(R.id.profileImageID);
        usernameText = findViewById(R.id.usernameID);

        firebaseAuth = FirebaseAuth.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference().child("profileImage");
        databaseReferenceUsers = FirebaseDatabase.getInstance().getReference().child("Users");
        databaseReference = FirebaseDatabase.getInstance().getReference("InstaApp");


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                onResume();
            }
        });

        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (postKey != null) {
            databaseReference.child(postKey).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    postUID = (String) dataSnapshot.child("uid").getValue();
                    postUsername = (String) dataSnapshot.child("username").getValue();

                    databaseReferenceUsers.child(postUID).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            profileImage = (String) dataSnapshot.child("image").getValue();

                            myAdapter.addImage(profileImage);
                            myAdapter.notifyDataSetChanged();
                            usernameText.setText(postUsername);
                            circleImageView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                            Picasso.with(ProfileActivity.this).load(profileImage).fit().into(circleImageView);
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
        final Handler handler = new Handler();
        final Thread r = new Thread() {
            public void run() {
                // DO WORK
                // Call function.
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fetchData();
                    }
                }, 1000);
            }
        };
        r.start();
        myAdapter = new RecyclerViewAdapter(postsArr, profileImage);
        rvProfilePosts.setAdapter(myAdapter);
    }

    public void fetchData(){

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                postsArr.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Insta post = postSnapshot.getValue(Insta.class);
                    String postKey = postSnapshot.getKey();

                    if (post != null && post.getUsername().equals(postUsername)) {
                        Map<String, Insta> map = new HashMap<>();
                        map.put(postKey, post);
                        postsArr.add(map);
                    }
                }
                onItemLoadComplete();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void onItemLoadComplete(){
        myAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }

    // Change profile picture

    public void profileImageButtonClicked(View view) {
        if (firebaseAuth.getCurrentUser().getUid().equals(postUID) || firebaseAuth.getCurrentUser().getUid().equals(userKey)) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, GALLERY_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                GALLERY_REQUEST);

        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            CropImage.activity(uri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();
                circleImageView.setImageURI(resultUri);
                Picasso.with(getApplicationContext()).load(resultUri).fit().into(circleImageView);
                final String userID = firebaseAuth.getCurrentUser().getUid();
                StorageReference filePath = storageReference.child(resultUri.getLastPathSegment());
                filePath.putFile(resultUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        String downloadurl = taskSnapshot.getDownloadUrl().toString();
                        databaseReferenceUsers.child(userID).child("image").setValue(downloadurl).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(ProfileActivity.this, "Profile picture is updated!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                    }
                });

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
