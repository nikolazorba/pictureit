package com.example.zorba.instaapp.InstagramREST.activity;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;


import com.example.zorba.instaapp.InstagramREST.POJO.Data;
import com.example.zorba.instaapp.InstagramREST.POJO.DataViewModel;
import com.example.zorba.instaapp.InstagramREST.POJO.InstagramResponse;
import com.example.zorba.instaapp.InstagramREST.adapter.FeedAdapter;
import com.example.zorba.instaapp.InstagramREST.repository.DataRepository;
import com.example.zorba.instaapp.InstagramREST.repository.ViewModelFactory;
import com.example.zorba.instaapp.InstagramREST.rest.RestClient;
import com.example.zorba.instaapp.InstagramREST.rest.RetrofitService;
import com.example.zorba.instaapp.activity.BaseActivity;
import com.example.zorba.instaapp.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class FeedActivity extends BaseActivity {

    private EditText etSearch;
    private RecyclerView feedList;
    private List<Data> instaPhotos;
    private FeedAdapter feedAdapter;
    private String access_token = "";

    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    private DataViewModel dataViewModel;
    ViewModelFactory factory;


    @Override
    public Lifecycle getLifecycle() {
        return this.lifecycleRegistry;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        feedList = findViewById(R.id.feedListID);
        feedList.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        feedList.setLayoutManager(linearLayoutManager);

        instaPhotos = new ArrayList<>();

        feedAdapter = new FeedAdapter(instaPhotos);
        feedList.setAdapter(feedAdapter);


        // Get the access_token from the intent extra
        Intent i = this.getIntent();
        access_token = i.getStringExtra("access_token");

        etSearch = findViewById(R.id.et_search);

        etSearch.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if (etSearch.getText().length() <= 0) {
                        Log.e("ee", "manje od 0");
                        Toast.makeText(getApplicationContext(), "Enter a search tag", Toast.LENGTH_SHORT).show();
                    } else {
                        dataViewModel.init(etSearch.getText().toString(), access_token);
                        dataViewModel.getData().observe(FeedActivity.this, new Observer<InstagramResponse>() {
                            @Override
                            public void onChanged(@Nullable InstagramResponse data) {
                                if (data != null) {
//                                    instaPhotos = Arrays.asList(data.getData());
                                    instaPhotos.clear();
                                    instaPhotos.addAll(Arrays.asList(data.getData()));
                                    feedAdapter.notifyDataSetChanged();
                                    Log.e("fa", "rara312 " + instaPhotos.size());
                                }
                            }
                        });
                        //fetchData(etSearch.getText().toString());
                        etSearch.setText("");
                        etSearch.clearFocus();
                    }
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        ViewModelFactory factory = new ViewModelFactory(DataRepository.getInstance());
        dataViewModel = ViewModelProviders.of(this, factory).get(DataViewModel.class);
    }

}
