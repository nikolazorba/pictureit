package com.example.zorba.instaapp.dialog;

import android.content.Context;
import android.text.TextUtils;

import com.example.zorba.instaapp.POJO.Enums;

import java.util.List;

/**
 * Created by zorba on 27/12/2017.
 */

public class CustomDialogBuilder {

    private CustomDialog customDialog;

    // must have
    private Context context;
    private String messageString;
    private int messageStringResId;
    private Enums.DialogOption[] options;
    private boolean isCancelable = true;

    public CustomDialogBuilder(Context context){
        this.context = context;
    }

    public CustomDialogBuilder withMessageString(String messageString){
        this.messageString = messageString;
        return this;
    }

    public CustomDialogBuilder withMessageResId(int messageStringResId){
        this.messageStringResId = messageStringResId;
        return this;
    }

    public CustomDialogBuilder withOptions(Enums.DialogOption... options){
        this.options = options;
        return this;
    }

    public CustomDialogBuilder setCancelable(boolean isCancelable) {
        this.isCancelable = isCancelable;
        return this;
    }


    public CustomDialog build(CustomDialog.OnDialogCallback onDialogCallback){

        if(!TextUtils.isEmpty(messageString)){
            customDialog = new CustomDialog(context, messageString, onDialogCallback, options);
        }else{
            customDialog = new CustomDialog(context, messageStringResId, onDialogCallback, options);
        }
        customDialog.setCancelable(isCancelable);

        return customDialog;
    }
}
