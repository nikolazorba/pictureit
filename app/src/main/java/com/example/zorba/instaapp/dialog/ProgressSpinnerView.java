package com.example.zorba.instaapp.dialog;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Property;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

import com.example.zorba.instaapp.R;

public class ProgressSpinnerView extends View {
    private Paint paint;
    private RectF rectF;
    private float sweepAngle;
    private float startAngle;
    private float radius = 100;
    private static final int BACKGROUND = R.color.black_25_transparent;
    private static final long ANIM_DURATION = 600L;
    private static final long ROTATION_DURATION = 3000L;

    public ProgressSpinnerView(Context context) {
        super(context);
        init();
    }


    public ProgressSpinnerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProgressSpinnerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(12f);
        paint.setStrokeCap(Paint.Cap.BUTT);
        rectF = new RectF();
        startAngle = 270;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(getResources().getColor(BACKGROUND));
        float height = canvas.getHeight() / 2;
        float width = canvas.getWidth() / 2;
        rectF.set(width - radius, height - radius, width + radius, height + radius);
        canvas.drawArc(rectF, startAngle, sweepAngle, false, paint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        animateArch();
    }

    private void animateArch() {


        final ObjectAnimator sweepAngleAnimator = ObjectAnimator.ofFloat(this, SET_SWEEPWANGLE, 0, 360);
        final ObjectAnimator startAngleAnimator = ObjectAnimator.ofFloat(this, SET_STARTWANGLE, 0, 360);
        ObjectAnimator rotation = ObjectAnimator.ofFloat(this, SET_ROTATION, 0, 360);

        sweepAngleAnimator.setDuration(ANIM_DURATION);
        startAngleAnimator.setDuration(ANIM_DURATION);
        rotation.setDuration(ROTATION_DURATION);

        sweepAngleAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        sweepAngleAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        rotation.setInterpolator(new LinearInterpolator());

        sweepAngleAnimator.start();
        rotation.setRepeatCount(Animation.INFINITE);
        rotation.start();
        sweepAngleAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                Log.e("onAnimationEnd", " is called!");
                startAngleAnimator.start();

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        startAngleAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                sweepAngleAnimator.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public static final Property<ProgressSpinnerView, Float> SET_SWEEPWANGLE =
            new Property<ProgressSpinnerView, Float>(Float.class, "sweepAngleProgress") {
                @Override
                public Float get(ProgressSpinnerView object) {
                    return object.getSweepAngle();
                }

                @Override
                public void set(ProgressSpinnerView object, Float value) {
                    object.setSweepAngle(value);
                    Log.e("object" , "sweepAngle " + object.getSweepAngle());
                    Log.e("object" , "startAngle " + object.getStartAngle());
                }
            };

    public float getSweepAngle() {
        return sweepAngle;
    }

    public void setSweepAngle(float sweepAngle) {
        this.sweepAngle = sweepAngle;
        postInvalidate();
    }



    public static final Property<ProgressSpinnerView, Float> SET_STARTWANGLE =
            new Property<ProgressSpinnerView, Float>(Float.class, "startAngleProgress") {
                @Override
                public Float get(ProgressSpinnerView object) {
                    return object.getStartAngle();
                }

                @Override
                public void set(ProgressSpinnerView object, Float value) {
                    float currentPosition = object.getStartAngle();
                    object.setStartAngle(currentPosition + value);
                    object.setSweepAngle(360f - value);

                    Log.e("anim2" , "startAngle " + object.getStartAngle());
                    Log.e("anim2" , "sweepAngle " + object.getSweepAngle());
                }
            };

    public static final Property<ProgressSpinnerView, Float> SET_ROTATION =
            new Property<ProgressSpinnerView, Float>(Float.class, "rotation") {
                @Override
                public Float get(ProgressSpinnerView object) {
                    return object.getStartAngle();
                }

                @Override
                public void set(ProgressSpinnerView object, Float value) {

                    object.setStartAngle(value);

                    Log.e("anim2" , "startAngle " + object.getStartAngle());
                    Log.e("anim2" , "sweepAngle " + object.getSweepAngle());
                }
            };

    public float getStartAngle() {
        return startAngle;
    }

    public void setStartAngle(float startAngle) {
        this.startAngle = startAngle;
        postInvalidate();
    }
}

