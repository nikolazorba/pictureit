package com.example.zorba.instaapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zorba.instaapp.POJO.Insta;
import com.example.zorba.instaapp.R;
import com.example.zorba.instaapp.activity.SinglePostActivity;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Map;

/**
 * Created by zorba on 20/11/2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private List<Map<String, Insta>> values;
    private String profiles;

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView postDescription;
        private TextView postUsername;
        private ImageView postImage;
        private CircularImageView profileImage;
        private View layout;


        public ViewHolder(View mView) {
            super(mView);
            layout = mView;
            postDescription = layout.findViewById(R.id.textDescription);
            postImage = (ImageView) layout.findViewById(R.id.post_image);
            postUsername = layout.findViewById(R.id.textUsername);
            profileImage = layout.findViewById(R.id.userProfileImageID);
        }
    }

    public void add(int position, Map<String, Insta> post) {
        values.add(position, post);
        notifyItemInserted(position);
    }

    public void addImage(String profileImage) {
        profiles = profileImage;
    }

    public void remove(int position) {
        values.remove(position);
        notifyItemRemoved(position);
    }


    public RecyclerViewAdapter(List<Map<String, Insta>> map, String profileImage) {
        values = map;
        profiles = profileImage;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.card_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Map<String, Insta> instaMap = values.get(position);
        Map.Entry<String, Insta> enrty = instaMap.entrySet().iterator().next();
        final Insta insta = enrty.getValue();
        final String postKey = enrty.getKey();
        holder.postUsername.setText(insta.getUsername());
        final Context context = holder.postImage.getContext();
        holder.postDescription.setText(insta.getDesc());
        Picasso.with(context).load(insta.getImage()).into(holder.postImage);

        //final String profileImage = profiles.get(position);
        final Context context1 = holder.profileImage.getContext();
        Picasso.with(context1)
                .load(profiles)
                .resize(100, 100)
                .centerInside()
                .into(holder.profileImage);


        holder.postImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent singlePostActivity = new Intent(holder.layout.getContext(), SinglePostActivity.class);
                singlePostActivity.putExtra("PostID", postKey);
                view.getContext().startActivity(singlePostActivity);
            }
        });
    }

    @Override
    public int getItemCount() {
        return values.size();
    }
}
