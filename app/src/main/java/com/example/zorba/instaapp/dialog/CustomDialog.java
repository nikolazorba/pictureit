package com.example.zorba.instaapp.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.zorba.instaapp.POJO.Enums;
import com.example.zorba.instaapp.R;

public class CustomDialog extends Dialog{

    private Enums.DialogOption[] options;
    private TextView textView;
    private OnDialogCallback onDialogCallback;
    private String messageString;
    private int messageResId;


    public interface OnDialogCallback{
        void onButtonPressed(Enums.DialogOption buttonPressed);
    }

    CustomDialog(Context context, String messageString, OnDialogCallback onDialogCallback, Enums.DialogOption... options) {
        super(context);
        this.messageString = messageString;
        this.options = options;
        this.onDialogCallback = onDialogCallback;
        show();
    }

    CustomDialog(Context context, int messageResId, OnDialogCallback onDialogCallback, Enums.DialogOption... options) {
        super(context);
        this.messageResId = messageResId;
        this.options = options;
        this.onDialogCallback = onDialogCallback;
        show();
    }

    @Override
    public void setCancelable(boolean flag) {
        super.setCancelable(flag);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom);

        textView = findViewById(R.id.messageDialogID);
        if(!TextUtils.isEmpty(messageString)){
            textView.setText(messageString);
        } else {
            textView.setText(messageResId);
        }

        LinearLayout linearLayout = findViewById(R.id.layout_dialog_buttons);
        for(final Enums.DialogOption option : options){
            final Button btnOption = (Button) getLayoutInflater().inflate(R.layout.dialog_button, linearLayout, false);
            btnOption.setText(option.toString());
            btnOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                     if(onDialogCallback != null){
                        onDialogCallback.onButtonPressed(option);
                    }
                    dismiss();
                }
            });
            linearLayout.addView(btnOption);
        }
    }
}
