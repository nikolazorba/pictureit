package com.example.zorba.instaapp.InstagramREST.POJO;

/**
 * Created by zorba on 16/01/2018.
 */

public class InstagramResponse {

    private Data[] data;

    public Data[] getData() {
        return data;
    }

    public void setData(Data[] data) {
        this.data = data;
    }
}
