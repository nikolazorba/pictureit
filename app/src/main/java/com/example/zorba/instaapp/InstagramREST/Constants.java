package com.example.zorba.instaapp.InstagramREST;

/**
 * Created by torzsacristian on 29/06/2017.
 */

public final class Constants {
    public static final String BASE_URL = "https://api.instagram.com/";
    public static final String CLIENT_ID = "d47cdb2c56124c57b6ba3387e12ccb8a";
    public static final String REDIRECT_URI = "https://www.instagram.com/";
}
