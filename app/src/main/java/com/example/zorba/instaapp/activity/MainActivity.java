package com.example.zorba.instaapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.zorba.instaapp.POJO.Enums;
import com.example.zorba.instaapp.dialog.CustomDialog;
import com.example.zorba.instaapp.POJO.Insta;
import com.example.zorba.instaapp.R;
import com.example.zorba.instaapp.dialog.CustomDialogBuilder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class MainActivity extends BaseActivity implements View.OnClickListener {

    private RecyclerView rvInstaList;
    private DatabaseReference databaseReference;
    private DatabaseReference databaseReferenceUsers;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    FirebaseUser firebaseUser;
    private String userKey;
    private TextView logout;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar);
        View view = getSupportActionBar().getCustomView();

        logout = view.findViewById(R.id.logout_text);
        logout.setOnClickListener(this);

        rvInstaList = findViewById(R.id.insta_list);
        rvInstaList.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        rvInstaList.setLayoutManager(linearLayoutManager);

        databaseReference = FirebaseDatabase.getInstance().getReference().child("InstaApp");
        databaseReferenceUsers = FirebaseDatabase.getInstance().getReference().child("Users");

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                if (firebaseAuth.getCurrentUser() == null) {

                    Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(loginIntent);
                } else {
                    userKey = firebaseAuth.getCurrentUser().getUid();
                }
            }
        };

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayoutMain);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                onResume();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }


    @Override
    protected void onResume() {
        super.onResume();
        firebaseAuth.addAuthStateListener(authStateListener);
        FirebaseRecyclerAdapter<Insta, InstaViewHolder> recyclerAdapter = new FirebaseRecyclerAdapter<Insta, InstaViewHolder>(
                Insta.class,
                R.layout.card_view,
                InstaViewHolder.class,
                databaseReference) {
            @Override
            protected void populateViewHolder(final InstaViewHolder viewHolder, Insta model, int position) {
                final String postKey = getRef(position).getKey().toString();

                viewHolder.setDescription(model.getDesc());
                viewHolder.setImage(getApplicationContext(), model.getImage());
                viewHolder.setUsername(model.getUsername());

                databaseReference.child(postKey).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        final String postUID = (String) dataSnapshot.child("uid").getValue();
                        databaseReferenceUsers.child(postUID).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                final String profileImage = (String) dataSnapshot.child("image").getValue();
                                viewHolder.setProfileImage(getApplicationContext(), profileImage);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }


                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class);
                        profileIntent.putExtra("PostID", postKey);
                        startActivity(profileIntent);
                    }
                });
            }
        };
        rvInstaList.setAdapter(recyclerAdapter);
        // postKey = firebaseUser.getUid();

    }



    @Override
    public void onBackPressed() {
        new CustomDialogBuilder(this).withMessageResId(R.string.exit_message)
                .withOptions(Enums.DialogOption.YES, Enums.DialogOption.NO)
                .build(new CustomDialog.OnDialogCallback() {
                    @Override
                    public void onButtonPressed(Enums.DialogOption buttonPressed) {
                        if (buttonPressed == Enums.DialogOption.YES) {
                            finishAffinity();
                        }
                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.logout_text:
                firebaseAuth.signOut();
        }
    }
}


