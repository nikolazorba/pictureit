package com.example.zorba.instaapp.InstagramREST.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.zorba.instaapp.InstagramREST.POJO.Data;
import com.example.zorba.instaapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by torzsacristian on 29/06/2017.
 */

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder>{

    private List<Data> values;

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView postUsername;
        private ImageView postImage;
        private View layout;


        public ViewHolder(View mView) {
            super(mView);
            layout = mView;
            postImage =  layout.findViewById(R.id.post_image);
            postUsername = layout.findViewById(R.id.textUsername);
        }
    }

    public FeedAdapter(List<Data> data){
        values = data;
    }

    @Override
    public FeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.card_view, parent, false);
        FeedAdapter.ViewHolder vh = new FeedAdapter.ViewHolder(v);
        return vh;
    }

    public void add(int position, Data data) {
        values.add(position, data);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        values.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final Data data = values.get(position);
        holder.postUsername.setText(data.getUser().getFull_name());
        final Context context = holder.postImage.getContext();
        Picasso.with(context)
                .load(data.getImages().getStandard_resolution().getUrl())
                .into(holder.postImage);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }
}

