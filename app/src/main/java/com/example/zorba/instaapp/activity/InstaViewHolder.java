package com.example.zorba.instaapp.activity;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zorba.instaapp.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;


/**
 * Created by zorba on 18/11/2017.
 */


public class InstaViewHolder extends  RecyclerView.ViewHolder{

    View mView;

    public InstaViewHolder(View itemView){
        super(itemView);
        mView = itemView;
    }

    public void setDescription(String description){
        TextView postDescription = mView.findViewById(R.id.textDescription);
        postDescription.setText(description);
    }

    public void setImage(Context ctx, String image){
        ImageView postImage = (ImageView) mView.findViewById(R.id.post_image);
        Picasso.with(ctx).load(image).into(postImage);
    }

    public void setUsername(String username){
        TextView postUsername = mView.findViewById(R.id.textUsername);
        postUsername.setText(username);
    }

    public void setProfileImage(Context ctx, String image){
        CircularImageView profileImage = mView.findViewById(R.id.userProfileImageID);
        Picasso.with(ctx)
                .load(image)
                .resize(100, 100)
                .centerInside()
                .into(profileImage);
    }
}