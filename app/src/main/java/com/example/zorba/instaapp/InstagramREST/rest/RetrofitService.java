package com.example.zorba.instaapp.InstagramREST.rest;




import com.example.zorba.instaapp.InstagramREST.POJO.Data;
import com.example.zorba.instaapp.InstagramREST.POJO.InstagramResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by torzsacristian on 26/02/2017.
 */

public interface RetrofitService {

    @GET("v1/tags/{tag_name}/media/recent")
    Call<InstagramResponse> getTagPhotos(@Path("tag_name") String tag_name,
                                         @Query("access_token") String access_token);
}

