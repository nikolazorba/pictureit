package com.example.zorba.instaapp.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zorba.instaapp.BottomNavigationViewHelper;
import com.example.zorba.instaapp.InstagramREST.dialog.AuthenticationDialog;
import com.example.zorba.instaapp.InstagramREST.listener.AuthenticationListener;
import com.example.zorba.instaapp.POJO.Enums;
import com.example.zorba.instaapp.R;
import com.example.zorba.instaapp.dialog.CustomDialog;
import com.example.zorba.instaapp.dialog.CustomDialogBuilder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class SinglePostActivity extends BaseActivity implements AuthenticationListener {

    private String postKey = null;

    private TextView singleUsername;
    private ImageView singleImage;
    private TextView singleDesc;
    private Button deleteButton;

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private FirebaseUser firebaseUser;
    private String userKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_post);

        postKey = getIntent().getExtras().getString("PostID");

        singleUsername = findViewById(R.id.singleUsernameID);
        singleImage = findViewById(R.id.singleImageID);
        singleDesc = findViewById(R.id.singleDescID);
        deleteButton = findViewById(R.id.deleteButtonID);
        deleteButton.setVisibility(View.INVISIBLE);

        firebaseAuth = FirebaseAuth.getInstance();
        userKey = firebaseAuth.getCurrentUser().getUid();


        databaseReference = FirebaseDatabase.getInstance().getReference().child("InstaApp");
        databaseReference.child(postKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String username = (String) dataSnapshot.child("username").getValue();
                String postDesc = (String) dataSnapshot.child("desc").getValue();
                String postImage = (String) dataSnapshot.child("image").getValue();
                String postUID = (String) dataSnapshot.child("uid").getValue();

                singleUsername.setText(username);
                singleDesc.setText(postDesc);

                Picasso.with(SinglePostActivity.this).load(postImage).into(singleImage);

                if (firebaseAuth.getCurrentUser().getUid().equals(postUID)) {
                    deleteButton.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void deleteButtonClicked(View view){
        new CustomDialogBuilder(this).withMessageResId(R.string.dialog_message_deleted_picture).withOptions(Enums.DialogOption.OK)
                .build(new CustomDialog.OnDialogCallback() {
            @Override
            public void onButtonPressed(Enums.DialogOption buttonPressed) {
                databaseReference.child(postKey).removeValue();
                Intent mainIntent = new Intent(SinglePostActivity.this, MainActivity.class);
                startActivity(mainIntent);
            }
        });


    }
}
