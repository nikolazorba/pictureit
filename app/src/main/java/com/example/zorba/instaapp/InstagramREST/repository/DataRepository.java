package com.example.zorba.instaapp.InstagramREST.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;
import android.widget.Toast;

import com.example.zorba.instaapp.InstagramREST.POJO.Data;
import com.example.zorba.instaapp.InstagramREST.POJO.InstagramResponse;
import com.example.zorba.instaapp.InstagramREST.rest.RestClient;
import com.example.zorba.instaapp.InstagramREST.rest.RetrofitService;

import java.util.List;

import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.instabug.library.Instabug.getApplicationContext;

@Singleton
public class DataRepository {

    private static DataRepository mInstance;

    private DataRepository(){
    }

    public static DataRepository getInstance(){
        if(mInstance == null){
            mInstance = new DataRepository();
        }
        return  mInstance;
    }



    public LiveData<InstagramResponse> getData(String tag, String access_token){
        final MutableLiveData<InstagramResponse> data = new MutableLiveData<>();

        Call<InstagramResponse> call = RestClient.getInstance().getService(RetrofitService.class).getTagPhotos(tag, access_token);
        call.enqueue(new Callback<InstagramResponse>() {
            @Override
            public void onResponse(Call<InstagramResponse> call, Response<InstagramResponse> response) {

                if (response.body() != null) {
                        data.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<InstagramResponse> call, Throwable t) {
                //Handle failure
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
            }
        });

        return data;
    }
}
