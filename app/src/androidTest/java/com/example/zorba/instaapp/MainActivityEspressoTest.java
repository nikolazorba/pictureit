package com.example.zorba.instaapp;

import android.os.SystemClock;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.FailureHandler;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.example.zorba.instaapp.activity.MainActivity;
import com.google.firebase.auth.FirebaseAuth;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.registerLooperAsIdlingResource;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by zorba on 15/12/2017.
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityEspressoTest {

    private FirebaseAuth firebaseAuth;
    private static final String EMAIL = "aleksandra@gmail.com";
    private static final String PASSWORD = "aleksandra94";
    private static final String SEARCH_TEXT = "winter";


    @Rule
    public ActivityTestRule<MainActivity> main = new ActivityTestRule<>(MainActivity.class);



    @Test
    public void mainTest(){

        firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() == null) {
            onView(withId(R.id.googleSignIn)).perform(click());
            SystemClock.sleep(3000);
        }

        onView(withId(R.id.tab_search)).perform(click());
        SystemClock.sleep(3000);
        onView(withId(R.id.et_search)).perform(typeText(SEARCH_TEXT));
        onView(withId(R.id.et_search)).perform(pressImeActionButton());
        SystemClock.sleep(2000);
        Espresso.pressBack();

        onView(withId(R.id.insta_list)).perform(RecyclerViewActions.scrollToPosition(0));
        onView(withId(R.id.insta_list)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        onView(withId(R.id.profilePostsID)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        onView(withText(R.string.delete_image)).withFailureHandler(new FailureHandler() {
            @Override
            public void handle(Throwable error, Matcher<View> viewMatcher){
            }
        }).check(matches(isDisplayed())).perform(click());


        onView(withId(R.id.tab_home)).perform(click());

        onView(withId(R.id.logout_text)).perform(click());

        onView(withId(R.id.link_signup)).perform(click());
        Espresso.pressBack();
        onView(withId(R.id.emailInputID)).perform(typeText(EMAIL));
        onView(withId(R.id.passInputID)).perform(typeText(PASSWORD));
        onView(withId(R.id.btn_login)).perform(click());
        SystemClock.sleep(3000);
        onView(withId(R.id.tab_share)).perform(click());
        Espresso.pressBack();


    }
}
